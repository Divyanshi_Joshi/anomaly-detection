# Anomaly Detection


Welcome to the Anomaly Detection repository! This repository contains a comprehensive study and implementation of various machine learning techniques for anomaly detection.

## Contents

1. **Presentation Deck**: A detailed presentation on anomaly detection and the machine learning techniques that can be used to achieve it.

2. **Implementation Notebooks**: Three Colab notebooks, each demonstrating the implementation of a machine learning algorithm from supervised, unsupervised, and semi-supervised learning. These notebooks tackle a real-life example of credit card fraud detection.

## Machine Learning Techniques

Anomalies are patterns in data that do not conform to a well defined notion of normal behavior. In other words, we can think of them as data coming from another generating process than the one most observations came from.  

To detect such outliers, the following machine learning techniques are explored in this repository:

1. **Supervised Learning**: Supervised learning is a type of machine learning algorithm that uses labeled data to learn the relationship between input and output.

2. **Unsupervised Learning**: Unsupervised learning is a type of machine learning algorithm used to draw inferences from datasets consisting of input data without labeled responses.

3. **Semi-Supervised Learning**: Semi-supervised learning is a class of machine learning tasks and techniques that also make use of unlabeled data for training – typically a small amount of labeled data with a large amount of unlabeled data.

## Real-Life Example: Credit Card Fraud Detection

The implementation notebooks in this repository demonstrate how these machine learning techniques can be applied to a real-life problem: credit card fraud detection. Each notebook tackles one machine learning algorithm and applies it to a dataset of credit card transactions to detect fraudulent activity.
